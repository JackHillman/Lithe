var path = require('path');

module.exports = {
  entry: {
    'admin': './assets/scripts/admin.js',
    'lithe': './assets/scripts/lithe.js',
  },
  output: {
    path: path.join(__dirname, 'dist/scripts'),
    filename: '[name].js',
  },
  module: {
    loaders: [{
      loader: 'babel-loader',
      query: {
        plugins: ['transform-runtime'],
        presets: ['es2015'],
      },
    }],
  },
  devtool: 'source-map',
};
