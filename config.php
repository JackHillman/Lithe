<?php

define('LITHE_VERSION', '0.1.0');
define('LITHE_ENTRYPOINT', __DIR__ . '/lithe.php');
define('LITHE_ROOT', __DIR__);
define('LITHE_SRC_DIR', __DIR__ . '/src/');
define('LITHE_VIEWS_DIR', __DIR__ . '/src/Views/');
define('LITHE_DIST_DIR', __DIR__ . '/dist/');
