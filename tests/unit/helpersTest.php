<?php

namespace Lithe;

class HelperTest extends \WP_UnitTestCase
{
    public function testViewLoaderPrintsView()
    {
        ob_start();
        require(LITHE_SRC_DIR . 'Views/Admin/Settings.php');
        $expected = ob_get_clean();

        ob_start();
        view('Admin/Settings');
        $actual = ob_get_clean();

        $this->assertEquals(
            $expected,
            $actual,
            "View helper function isn't printing view"
        );

        ob_start();
        view('Admin.Settings');
        $actual = ob_get_clean();

        $this->assertEquals(
            $expected,
            $actual,
            "View helper function doesn't work with dot notation"
        );
    }

    public function testAssetFunctionGetsCorrectAsset()
    {
        $this->assertEquals(
            plugins_url(basename(LITHE_ROOT)) . '/dist/scripts/test.js',
            asset('test.js'),
            "Asset helper doesn't return correct script"
        );

        $this->assertEquals(
            plugins_url(basename(LITHE_ROOT)) . '/dist/styles/test.css',
            asset('test.css'),
            "Asset helper doesn't return correct style"
        );

        $this->assertEquals(
            plugins_url(basename(LITHE_ROOT)) . '/dist/image.jpg',
            asset('image.jpg'),
            "Asset helper is returning incorrect path for unknown file type"
        );
    }
}
