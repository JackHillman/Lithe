<?php

use Lithe\Field;

class TestField extends Field
{
    public function validate()
    {
        return (bool) !$this->required || ($this->required && $this->value);
    }

    public function render()
    {
        ?>
        <?php if ($this->name) : ?>
            <label for="<?= $this->id(); ?>"><?= $this->name; ?></label>
        <?php endif; ?>
        <input
            type="text"
            name="<?= $this->id(); ?>"
            id="<?= $this->slug; ?>"
            value="<?= $this->value ?: $this->default; ?>"
            <?= $this->required ? 'required' : ''; ?>>
        <?php
    }
}

class FieldTest extends WP_UnitTestCase
{
    public function testValuesCanBeSetViaMethods()
    {
        $textField = TestField::slug('textField')
                              ->name('My Text Field')
                              ->required();

        $this->assertEquals(
            'textField',
            $textField->slug,
            "Slug wasn't set correctly using __callStatic method"
        );

        $this->assertEquals(
            'My Text Field',
            $textField->name,
            "Name wasn't set correctly using __call method"
        );

        $this->assertTrue(
            $textField->required,
            "Required value wasn't set correctly using __class method"
        );
    }

    public function testFieldGetsItsClassCorrectly()
    {
        $textField = TestField::create();

        $this->assertTrue(
            $textField->isCurrentClass(TestField::class),
            "isCurrentClass is returning false for correct class"
        );

        $this->assertTrue(
            $textField->isCurrentClass(Field::class),
            "isCurrentClass is returning false for correct class"
        );

        $this->assertFalse(
            $textField->isCurrentClass('FakeClass'),
            "isCurrentClass is returning true for incorrect class"
        );
    }

    public function testFieldCanSaveMeta()
    {
        $field = $this->getMockForAbstractClass(Field::class)
                      ->slug('field')
                      ->name('Field');

        $this->assertEquals(
            'field',
            $field->getMeta()->slug,
            "Field meta retrieved slug incorrectly"
        );

        $this->assertEquals(
            'Field',
            $field->getMeta()->name,
            "Field meta retrieved name incorrectly"
        );

        $this->assertEquals(
            null,
            $field->getMeta()->value,
            "Field meta retrieved value incorrectly"
        );

        $this->assertEquals(
            get_class($field),
            $field->getMeta()->class,
            "Field meta retrieved class incorrectly"
        );
    }

    public function testFieldCanBeValidated()
    {
        $field = $this->getMockForAbstractClass(Field::class)
                      ->slug('field')
                      ->name('Field')
                      ->rules('required|max:255');

        $this->assertTrue($field->value('This is a valid value')->validate());
        $this->assertFalse($field->value(null)->validate());
        $this->assertFalse($field->value(str_repeat('a', 256))->validate());
    }
}
