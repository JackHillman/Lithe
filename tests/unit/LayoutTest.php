<?php

use Lithe\Layout;

class LayoutTest extends WP_UnitTestCase
{
    public function testLayoutCanBeCreated()
    {
        $layout = $this->getMockBuilder(Layout::class)
                       ->getMock();

        $this->assertEquals(
            12,
            $layout->columns[0]['width'],
            "Default layout width is incorrect"
        );
    }

    /**
     * @expectedException           Exception
     * @expectedExceptionMessage    Invalid column width
     */
    public function testLayoutValidatesColumnCount()
    {
        $layout = $this->getMockBuilder(Layout::class)
                       ->setMethods(null)
                       ->getMock();

        $layout->columns = [['width' => 11]];

        $layout->validateColumns();
    }
}
