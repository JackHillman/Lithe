<?php

use Lithe\Traits\Validator;
use Lithe\Exceptions\InvalidRuleException;
use Faker\Factory;

class ValidatorTest extends WP_UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->mock = $this->getMockForTrait(Validator::class);
        $this->faker = Factory::create();
    }

    public function testRulesCanBeSet()
    {
        $this->mock->rules = 'required';
        $this->assertEquals('required', $this->mock->rules);

        $this->mock->rules = 'max:255';
        $this->assertEquals('max:255', $this->mock->rules);

        $this->mock->rules = 'required|max:255';
        $this->assertEquals('required|max:255', $this->mock->rules);
    }

    public function testRulesCanBeBuilt()
    {
        $this->mock->rules = 'required';
        $this->assertEquals(['required' => true], $this->mock->buildRules());

        $this->mock->rules = 'max:255';
        $this->assertEquals(['max' => 255], $this->mock->buildRules());

        $this->mock->rules = 'required|max:255';
        $this->assertEquals(['required' => true, 'max' => 255], $this->mock->buildRules());
    }

    public function testRequiredRuleCanBeValidated()
    {
        $this->mock->rules = 'required';
        $this->assertFalse($this->mock->validate(null));
        $this->assertFalse($this->mock->validate(''));
        $this->assertFalse($this->mock->validate('0'));
        $this->assertFalse($this->mock->validate(0));
        $this->assertFalse($this->mock->validate(false));
        $this->assertTrue($this->mock->validate('Value'));
        $this->assertTrue($this->mock->validate(12345));
        $this->assertTrue($this->mock->validate(true));
    }

    public function testMaxLengthRuleCanBeValidated()
    {
        $this->mock->rules = 'max:255';
        $this->assertFalse($this->mock->validate(str_repeat($this->faker->lexify('?'), 256)));
        $this->assertTrue($this->mock->validate(str_repeat($this->faker->lexify('?'), 255)));
        $this->assertTrue($this->mock->validate(str_repeat($this->faker->lexify('?'), 254)));
        $this->assertTrue($this->mock->validate(''));
    }

    public function testMinLengthRuleCanBeValidated()
    {
        $this->mock->rules = 'min:255';
        $this->assertTrue($this->mock->validate(str_repeat($this->faker->lexify('?'), 256)));
        $this->assertFalse($this->mock->validate(str_repeat($this->faker->lexify('?'), 255)));
        $this->assertFalse($this->mock->validate(str_repeat($this->faker->lexify('?'), 254)));
        $this->assertFalse($this->mock->validate(''));
    }

    public function testValidatorWithoutRulesAlwaysPassesValidation()
    {
        $this->mock->rules = '';
        $this->assertTrue($this->mock->validate(str_repeat($this->faker->lexify('?'), 256)));
        $this->assertTrue($this->mock->validate(str_repeat($this->faker->lexify('?'), 255)));
        $this->assertTrue($this->mock->validate(str_repeat($this->faker->lexify('?'), 254)));
        $this->assertTrue($this->mock->validate(null));
        $this->assertTrue($this->mock->validate(''));
        $this->assertTrue($this->mock->validate('0'));
        $this->assertTrue($this->mock->validate(0));
        $this->assertTrue($this->mock->validate(false));
        $this->assertTrue($this->mock->validate('Value'));
        $this->assertTrue($this->mock->validate(12345));
        $this->assertTrue($this->mock->validate(true));
    }

    public function testNumericRuleCanBeValidated()
    {
        $this->mock->rules = 'numeric';
        $this->assertTrue($this->mock->validate(123));
        $this->assertTrue($this->mock->validate('123'));
        $this->assertTrue($this->mock->validate(0));
        $this->assertTrue($this->mock->validate('0'));
        $this->assertTrue($this->mock->validate(PHP_INT_MAX));
        $this->assertTrue($this->mock->validate((string) PHP_INT_MAX));
        $this->assertTrue($this->mock->validate(-123));
        $this->assertTrue($this->mock->validate('-123'));
        $this->assertTrue($this->mock->validate(PHP_INT_MIN));
        $this->assertTrue($this->mock->validate((string) PHP_INT_MIN));
        $this->assertFalse($this->mock->validate(null));
        $this->assertFalse($this->mock->validate(''));
        $this->assertFalse($this->mock->validate(false));
        $this->assertFalse($this->mock->validate(true));
        $this->assertFalse($this->mock->validate('Value'));
        $this->assertFalse($this->mock->validate($this->faker->text));
    }

    public function testEmailRuleCanBeVaildated()
    {
        $this->mock->rules = 'email';

        $validEmails = [
            'email@example.com',
            'firstname.lastname@example.com',
            'email@subdomain.example.com',
            'firstname+lastname@example.com',
            '1234567890@example.com',
            'email@example-one.com',
            '_______@example.com',
            'email@example.name',
            'email@example.museum',
            'email@example.co.jp',
            'firstname-lastname@example.com',
        ];

        $invalidEmails = [
            'plainaddress',
            '#@%^%#$@#$@#.com',
            '@example.com',
            'Joe Smith <email@example.com>',
            'email.example.com',
            'email@example@example.com',
            '.email@example.com',
            'email.@example.com',
            'email..email@example.com',
            'あいうえお@example.com',
            'email@example.com (Joe Smith)',
            'email@example',
            'email@-example.com',
            'email@111.222.333.44444',
            'email@example..com',
            'Abc..123@example.com',
            '"(),:;<>[\]@example.com',
            'just"not"right@example.com',
            'this\ is\"really\"not\\\\allowed@example.com'
        ];

        foreach ($validEmails as $email) {
            $this->assertTrue($this->mock->validate($email), "Failed asserting {$email} is a valid email");
        }

        foreach ($invalidEmails as $email) {
            $this->assertFalse($this->mock->validate($email), "Failed asserting {$email} is an invalid email");
        }
    }

    public function testURLRulesCanBeValidated()
    {
        $this->mock->rules = 'url';
        $this->assertTrue($this->mock->validate('https://www.google.com/'));
        $this->assertTrue($this->mock->validate('http://www.google.com/'));
        $this->assertTrue($this->mock->validate('//www.google.com/'));
        $this->assertTrue($this->mock->validate('www.google.com/'));
        $this->assertTrue($this->mock->validate('google.com/'));
        $this->assertTrue($this->mock->validate('google.com'));
        $this->assertFalse($this->mock->validate('google'));
        $this->assertFalse($this->mock->validate('googleDOTcom'));
        $this->assertFalse($this->mock->validate(123));
        $this->assertFalse($this->mock->validate(null));
        $this->assertFalse($this->mock->validate(true));
    }

    /**
     * @expectedException Lithe\Exceptions\InvalidRuleException
     */
    public function testExceptionGetsThrownIfAttemptingToUseInvalidRule()
    {
        $this->mock->rules = 'badrule';
        $this->mock->validate(null);
    }
}
