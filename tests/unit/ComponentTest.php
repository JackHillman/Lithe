<?php

use Lithe\Component;
use Lithe\Exceptions\InvalidComponentException;

class ComponentTest extends WP_UnitTestCase
{
    public function testComponentsCanBeRegisterd()
    {
        $component = $this->getMockForAbstractClass(Component::class);
        Component::registerComponents([$component]);

        $this->assertEquals(
            1,
            count(Component::getRegisteredComponents()),
            "Component is registering incorrect number of components"
        );
    }

    /**
     * @expectedException Lithe\Exceptions\InvalidComponentException
     */
    public function testInvalidComponentThrowError()
    {
        Component::registerComponents(['Fake\Component']);
    }

    public function testComponentInits()
    {
        $this->assertTrue(
            is_array(Component::init()),
            "New component isn't returning array of components"
        );
    }

    public function testComponentIsMissingTemplateWarningExists()
    {
        $component = $this->getMockForAbstractClass(Component::class);
        $component->registerHooks();

        ob_start();
        do_action('admin_notices');
        $warning = ob_get_clean();

        $this->assertContains(
            sprintf('Component %s is missing a template file', get_class($component)),
            $warning,
            "Component without template doesn't throw error"
        );

        $component->template = '/tmp/test-template.php';

        ob_start();
        do_action('admin_notices');
        $warning = ob_get_clean();

        $this->assertEmpty(
            $warning,
            "Component with a template is throwing a warning"
        );
    }
}
