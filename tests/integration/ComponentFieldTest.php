<?php

use Lithe\Component;
use Lithe\Field;

class ComponentFieldTest extends WP_UnitTestCase
{
    public function setUp()
    {
        $this->component = $this->getMockForAbstractClass(Component::class);
        $this->field = $this->getMockForAbstractClass(Field::class)
                            ->slug('field')
                            ->name('My Field')
                            ->value('Field');

        $this->component->fields = [$this->field];
    }

    public function testCompoentCanGetFieldValuesForExtraction()
    {
        $this->assertEquals(
            ['field' => 'Field'],
            $this->component->getFieldValues(),
            "getFieldValues isn't parsing values correctly"
        );
    }

    public function testLoadTemplateCorrectlyLoadsTemplate()
    {
        $this->component->template = '/tmp/template.php';

        $template = 'This is a test template';
        file_put_contents('/tmp/template.php', $template);

        ob_start();
        $this->component->loadTemplate();
        $template_output = ob_get_clean();

        $this->assertEquals(
            $template,
            $template_output,
            "Component isn't loading template correcly"
        );

        $this->component->template = '/tmp/template';

        ob_start();
        $this->component->loadTemplate();
        $template_output = ob_get_clean();

        $this->assertEquals(
            $template,
            $template_output,
            "Component isn't loading template correcly without extension"
        );

        unlink('/tmp/template.php');
    }

    public function testComponentGetsMeta()
    {
        $fieldData = (object) [
            'slug' => 'field',
            'name' => 'My Field',
            'value' => 'Field',
            'class' => get_class($this->field),
        ];

        $this->assertEquals(
            [$fieldData],
            $this->component->getMeta(),
            "Component doesn't get single field meta correctly"
        );

        $newField = $this->getMockForAbstractClass(Field::class)
                         ->slug('another_field')
                         ->name('Another Field');

        $this->component->fields[] = $newField;

        $this->assertEquals(
            [$fieldData, (object) [
                'slug' => 'another_field',
                'name' => 'Another Field',
                'value' => null,
                'class' => get_class($newField),
            ]],
            $this->component->getMeta(),
            "Component doesn't get multiple field meta correctly"
        );
    }
}
