<div class="wrapper">
    <h1><?php _e('Lithe Settings', 'lithe'); ?></h1>
    <small><?php printf(__('Lithe %s, running on WordPress %s', 'lithe'), LITHE_VERSION, get_bloginfo('version')); ?></small>

    <h2><?php _e('Registered Components', 'lithe'); ?></h2>
</div>
