<?php

namespace Lithe;

use Lithe\Traits\Validator;

abstract class Field
{
    use Validator {
        Validator::validate as validateValue;
    }

    /**
     * Field slug.
     * This will be the variable name in templates, and will be used to store the
     * field value in the database. Must be unique per component.
     * @var String
     */
    public $slug;

    /**
     * Field Name.
     * Name will usually be displayed only in the admin as a label.
     * @var String
     */
    public $name;

    /**
     * Field value.
     * If the field has been saved, this will contain the value.
     * @var Mixed
     */
    public $value;

    /**
     * Default field value.
     * NOTE Default field value will be returned if the $value is falsey.
     * @var String
     */
    public $default;

    public function validate()
    {
        return $this->validateValue($this->value);
    }

    /**
     * Render field in WordPress admin.
     */
    abstract public function render();

    /**
     * Get field meta to save to database
     */
    public function getMeta()
    {
        return (object) [
            'slug' => $this->slug,
            'name' => $this->name,
            'value' => $this->value ?: $this->default,
            'class' => get_class($this),
        ];
    }

    /**
     * Create new instance of class.
     * Fields should NEVER be created with the new keyword, usually it will
     * use the __callStatic function, but can use create if required.
     *
     * @return \Lithe\Field  New instance of the current class
     */
    public static function create()
    {
        $instance = new static;

        $instance->parent = $instance->getParentComponentClass();

        return $instance;
    }

    /**
     * Check if parameter is the current class.
     *
     * @param  String  $class   Class string to check if current class
     * @return boolean          True if $class is either the current, or a child class
     */
    public function isCurrentClass($class)
    {
        return ($class === self::class || $class === static::class);
    }

    /**
     * __call and __callStatic are the bread and butter of the Field class.
     * These can be used to chain set the values of the field.
     *
     * @param  String $name         Variable name
     * @param  Array  $arguments    Variable value
     * @return                      Current instance of \Lithe\Field
     */
    public function __call($name, $arguments)
    {
        $this->$name = empty($arguments) ? true : array_pop($arguments);

        return $this;
    }

    /**
     * Used to create the instance of the Field, __callStatic then passes its
     * arguments to __call.
     *
     * @param  String $name         Variable name
     * @param  Array  $arguments    Variable value
     * @return                      New instance of \Lithe\Field
     */
    public static function __callStatic($name, $arguments)
    {
        return $arguments ? static::create()->$name(array_pop($arguments)) : static::create()->$name();
    }
}
