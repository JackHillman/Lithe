<?php

namespace Lithe;

/**
 * Render a view, optionally with variables.
 *
 * @param  String $view         View file to be rendered, can use dot
 *                              notation and shouldn't contain an extension
 * @param  Variadic $variables  Variables to be passed through to view.
 */
function view($view, ...$variables)
{
    extract($variables);
    require LITHE_VIEWS_DIR . str_replace('.', '/', $view) . '.php';
}

/**
 * Get the url for an asset.
 *
 * @param String $asset     Asset filename.
 *
 * @return String           Full asset url.
 */
function asset($asset)
{
    $dist_folder = '';
    $asset_path = explode('.', $asset);
    switch (end($asset_path)) {
        case 'js':
            $dist_folder = 'scripts/';
            break;
        case 'css':
            $dist_folder = 'styles/';
            break;
    }

    return plugins_url(basename(LITHE_ROOT) . '/dist/' . $dist_folder . $asset);
}
