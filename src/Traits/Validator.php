<?php

namespace Lithe\Traits;

use Lithe\Exceptions\InvalidRuleException;

trait Validator
{
    public $rules = '';

    public $validationErrors = [];

    private $rulesHash = '';

    private $rulesArray = [];

    /**
     * Unfortunately PHP doesn't support constants in traits,
     * or enums in general, So instead these are just static variables
     */
    private static $required = 0;
    private static $max = 1;
    private static $min = 2;
    private static $numeric = 3;
    private static $email = 4;
    private static $url = 5;

    public function validate($value)
    {
        $rules = $this->maybeBuildRules();

        foreach ($rules as $rule => $requirement) {
            switch ($rule) {
                case 'required':
                    $this->validateRequired($value);
                    break;
                case 'max':
                    $this->validateMaxLength($value, $requirement);
                    break;
                case 'min':
                    $this->validateMinLength($value, $requirement);
                    break;
                case 'numeric':
                    $this->validateNumeric($value);
                    break;
                case 'email':
                    $this->validateEmail($value);
                    break;
                case 'url':
                    $this->validateURL($value);
                    break;
                case '': // Empty rule, AKA there are no rules
                    break;
                default:
                    throw new InvalidRuleException("{$rule} is not a valid rule");
            }
        }

        return !$this->hasErrors();
    }

    public function hasErrors()
    {
        return (count($this->validationErrors) > 0);
    }

    public function maybeBuildRules()
    {
        if ($this->rulesHash !== md5($this->rules)) {
            $this->buildRules();
        }

        return $this->rulesArray;
    }

    public function buildRules()
    {
        $this->rulesArray = [];
        $this->rulesHash = md5($this->rules);

        foreach (explode('|', $this->rules) as $rule) {
            $this->rulesArray = array_merge($this->rulesArray, $this->buildRule($rule));
        }

        return $this->rulesArray;
    }

    private function buildRule($rule)
    {
        $default = $match = ['key' => '', 'value' => ''];
        preg_match('/^(?<key>[^:]+)(:(?<value>.+))?/', $rule, $match);
        $match = array_merge($default, $match);

        return [$match['key'] => $match['value'] ?: true];
    }

    private function clearError($key)
    {
        if (isset($this->validationErrors[$key])) {
            unset($this->validationErrors[$key]);
        }
    }

    private function validateRequired($value)
    {
        $valid = (bool) $value;
        $this->clearError(self::$required);

        if (!$valid) {
            $this->validationErrors[self::$required] = __('Value is invalid');
        }

        return $valid;
    }

    private function validateMaxLength($value, $length)
    {
        $valid = strlen($value) <= $length;
        $this->clearError(self::$max);

        if (!$valid) {
            $this->validationErrors[self::$max] = sprintf(__('Value is larger than the max length of %s'), $length);
        }

        return $valid;
    }

    private function validateMinLength($value, $length)
    {
        $valid = strlen($value) > $length;
        $this->clearError(self::$min);

        if (!$valid) {
            $this->validationErrors[self::$min] = sprintf(__('Value is smaller than the min length of %s'), $length);
        }

        return $valid;
    }

    private function validateNumeric($value)
    {
        $valid = is_numeric($value);
        $this->clearError(self::$numeric);

        if (!$valid) {
            $this->validationErrors[self::$numeric] = __('Value is not numeric');
        }

        return $valid;
    }

    private function validateEmail($value)
    {
        $valid = filter_var($value, FILTER_VALIDATE_EMAIL);
        $this->clearError(self::$email);

        if (!$valid) {
            $this->validationErrors[self::$email] = __('Value is not a valid email');
        }

        return $valid;
    }

    private function validateURL($value)
    {
        $valid = preg_match('/(?:https?:\/\/)?(?:[a-zA-Z0-9.-]+?\.(?:[a-zA-Z])|\d+\.\d+\.\d+\.\d+)/', $value);
        $this->clearError(self::$url);

        if (!$valid) {
            $this->validationErrors[self::$url] = __('Value is not a valid URL');
        }

        return $valid;
    }
}
