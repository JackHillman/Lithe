<?php

namespace Lithe;

abstract class Layout
{
    public $name;

    public $columns = [
        [
            'width' => 12,
            'classes' => ['lithe-layout'],
        ]
    ];

    public function __construct()
    {
        $this->validateColumns();
    }

    public function validateColumns()
    {
        $count = 0;

        foreach ($this->columns as $column) {
            $count += $column['width'];
        }

        if ($count !== 12) {
            throw new \Exception('Invalid column width, column widths should total 12');
        }
    }
}
