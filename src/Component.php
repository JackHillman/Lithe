<?php

namespace Lithe;

use Lithe\Exceptions\InvalidComponentException;

abstract class Component
{
    /**
     * Template file location, can include an extension
     * @var String
     */
    public $template;

    /**
     * Field objects, these will be editable in the WordPress admin, and they
     * are what will be available in the templte.
     * @var Array
     */
    public $fields = [];

    /**
     * Registered components
     * @var Array
     */
    private static $components = [];

    /**
     * Minimum column size for this component.
     * @var Int
     */
    public $minSize = 0;

    abstract public function render();

    public function getMeta()
    {
        $fieldMeta = [];

        foreach ($this->fields as $field) {
            $fieldMeta[] = $field->getMeta();
        }

        return $fieldMeta;
    }

    /**
     * Register any hooks for the class
     *
     * @return \Lithe\Component  Instance of component
     */
    public static function register()
    {
        $instance = new static;

        $instance->registerHooks();

        return $instance;
    }

    public function registerHooks()
    {
        add_action('admin_notices', [$this, 'displayWarnings']);
    }

    /**
     * We register this from the constructor, this should tell the user/developer
     * that they are missing important functionality. Optional warnings should not
     * appear every time.
     *
     * TODO Implement one-time warnings
     */
    final public function displayWarnings()
    {
        if (!$this->template) :
        ?>
        <div class="notice notice-warning is-dismissible">
            <p><?php printf(__('Component %s is missing a template file.', 'lithe'), static::class); ?></p>
        </div>
        <?php
        endif;
    }

    /**
     * Extract variables and load in template for component
     */
    final public function loadTemplate()
    {
        extract($this->getFieldValues());

        if (file_exists($this->template)) {
            require $this->template;
        } elseif (file_exists($this->template . '.php')) {
            require $this->template . '.php';
        }
    }

    /**
     * Return an array of field slugs and values for extraction to template
     *
     * @return Array    Field slug => value pairs
     */
    final public function getFieldValues()
    {
        $values = [];

        foreach ($this->fields as $field) {
            $values[$field->slug] = $field->value ?: $field->default;
        }

        return $values;
    }

    /**
     * Abstract parent class init function.
     * We can't create a new instance of an abstract class (obviously) we are
     * going to use this as our entrypoint. We don't want to init the class too
     * early, otherwise it will be hard to register components. plugins_loaded is
     * fine.
     *
     * @return Array    Array of registered component instances.
     */
    final public static function init()
    {
        self::registerComponents(apply_filters('lithe_register_components', []));

        return self::$components;
    }

    /**
     * This will loop through all the components marked to be registered,
     * check that they are actual components (extend this class), then register
     * them for use.
     *
     * @param  Array    $components     Array of potential compoents
     */
    final public static function registerComponents($components)
    {
        foreach ($components as $component) {
            if (is_subclass_of($component, __CLASS__)) {
                self::$components[] = $component::register();
            } else {
                throw new InvalidComponentException(
                    "Component {$component} either doesn't exists, or doesn't extend " . __CLASS__
                );
            }
        }
    }

    /**
     * Getter for registered component, returns all components.
     *
     * @return Array    Array of registered component instances.
     */
    final public static function getRegisteredComponents()
    {
        return self::$components;
    }
}
