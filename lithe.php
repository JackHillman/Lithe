<?php
/**
 * Plugin Name:     Lithe
 * Plugin URI:      https://www.lithe.com
 * Description:     A flexible, flexible content solution for professionals
 * Author:          Jack Hillman
 * Author URI:      https://jackhillman.com.au
 * Text Domain:     lithe
 * Domain Path:     /languages
 * Version:         1.0.0-alpha
 *
 * @package         Lithe
 */

require_once 'bootstrap.php';

class Lithe
{
    public function __construct()
    {
        add_action('admin_menu', [$this, 'addOptionsPage']);
        add_action('admin_enqueue_scripts', [$this, 'enqueueAssets']);
    }

    public function addOptionsPage()
    {
        add_submenu_page(
            'options-general.php', // Parent page
            'Lithe Settings', // Page Title
            'Lithe Settings', // Menu Title
            'read', // Required capabilities
            'lithe', // Slug
            [$this, 'renderSettingsPage'] // Render callback
        );
    }

    public function renderSettingsPage()
    {
        Lithe\view('Admin.Settings');
    }

    public function enqueueAssets()
    {
        wp_enqueue_script('lithe-admin', Lithe\asset('admin.js'));
        wp_enqueue_style('lithe-admin', Lithe\asset('admin.css'));
    }
}

new Lithe();
