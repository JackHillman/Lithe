'use strict';

let clean = require('gulp-clean');
let gulp = require('gulp');
let gutil = require('gulp-util');
let runSequence = require('run-sequence');
let sass = require('gulp-sass');
let sourcemaps = require('gulp-sourcemaps');
let webpack = require('webpack');
let webpackConfig = require('./webpack.config.js');

gulp.task('webpack', (callback) => {
  webpack(webpackConfig, (err, stats) => {
    if (err) throw new gutil.PluginError('webpack', err);
    gutil.log('[webpack]', stats.toString());
  });
  callback();
});

gulp.task('styles', () => {
  return gulp.src('./assets/styles/admin.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed',
    }).on('error', sass.logError))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/styles'));
});

gulp.task('clean', () => {
  return gulp.src('./dist')
    .pipe(clean());
});

gulp.task('default', (callback) => {
  runSequence('clean', [
    'webpack',
    'styles',
  ], callback);
});
