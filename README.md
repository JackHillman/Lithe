# Lithe
## A flexible, flexible content solution for professionals
[![build status](https://gitlab.com/JackHillman/Lithe/badges/master/build.svg)](https://gitlab.com/JackHillman/Lithe/commits/master)
[![coverage report](https://gitlab.com/JackHillman/Lithe/badges/master/coverage.svg)](https://gitlab.com/JackHillman/Lithe/commits/master)

## Running Lithe
Lithe is a WordPress plugin, and as such must be installed in a working WordPress installation.

For existing sites, Lithe can be installed by dragging the `lithe` directory into your `wp-content/plugins` directory.

For working on Lithe, it is highly recommended to use Docker instead, see [Using docker-compose](#using-docker-compose) below for instructions

## Building
Building Lithe requires you to have [composer](https://getcomposer.org/) and [npm](https://www.npmjs.com/) (or [Yarn](https://yarnpkg.com/en/)) installed locally.

First get all PHP dependancies by running the below in your terminal.
```shell
$ composer install
```

Then install all Node dependencies:
```shell
$ npm install
```

Once all dependancies are downloaded, build the assets.
```shell
$ npm run build
```

## Using Docker Compose
To use [Docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/), please ensure both are installed and in your `$PATH`

### Starting Docker Compose Containers
Once they're installed, you can start the containers with the following command
```shell
$ docker-compose up -d
```

This will start up 3 containers. 1 WordPress container, and 2 MariaDB containers (one of PHPUnit, and another for WordPress)

### Accessing Docker Compose Containers
You will be able to view a working WordPress installation by visiting [http://localhost:8080](http://localhost:8080) in your browser.
The details are as follows:
```
Username: lithe
Password: lithe
```

### Installing Additional Plugins and Themes
The WordPress container will mount 2 additional volumes to allow installing additional plugins and themes.

To install these, drag their directory into either `docker/dev/plugins/` or `docker/dev/themes/` and activate them through the web interface.

These directories are ignored by git, and won't be tracked in the repo.

### Running Tests
The WordPress Docker container contains a working version of PHPUnit and WP CLI for testing, to run the test suite, paste the following into your terminal:
```shell
$ docker-compose exec wordpress tests
```

Any additional options can be passed after the command. E.g. to only run the tests in one file, you could run the following
```shell
$ docker-compose exec wordpress tests/ExampleTest.php
```

## Coding Standards
Lithe has strict coding standards, and will not build unless the standards are followed.
To ensure that your code meets this standards, we've included a pre-push hook to run the linters before you push code

To add this hook, run the following.
```shell
$ ln -s $PWD/pre-push $PWD/.git/hooks/
```

## Additional Resources
For general information, please visit [https://lithewp.com/](https://lithewp.com/)

For documentation, please visit [https://lithewp.com/docs/](https://lithewp.com/docs/)

For detailed code coverage, please visit [https://lithewp.com/coverage/](https://lithewp.com/coverage/)
